In iteration 0

test_rating, baseline_rating, (41.17647058823529, 54.42942942942943)
rating_mean, diversity_mean, (0.5882352941176471, 0.09062823350602091)


In iteration 1

test_rating, baseline_rating, (55.769230769230774, 54.42942942942943)
rating_mean, diversity_mean, (0.4423076923076923, 0.0640713216545605)


In iteration 2

test_rating, baseline_rating, (57.14285714285714, 54.42942942942943)
rating_mean, diversity_mean, (0.42857142857142855, 0.08357614702317372)


In iteration 3

test_rating, baseline_rating, (49.57983193277311, 54.42942942942943)
rating_mean, diversity_mean, (0.5042016806722689, 0.06319400360280264)


In iteration 4

test_rating, baseline_rating, (34.48275862068966, 54.42942942942943)
rating_mean, diversity_mean, (0.6551724137931034, 0.1455195474629899)


In iteration 5

test_rating, baseline_rating, (34.61538461538461, 54.42942942942943)
rating_mean, diversity_mean, (0.6538461538461539, 0.06514202879992811)


In iteration 6

test_rating, baseline_rating, (56.16438356164384, 54.42942942942943)
rating_mean, diversity_mean, (0.4383561643835616, 0.0934177492479277)


In iteration 7

test_rating, baseline_rating, (51.21951219512195, 54.42942942942943)
rating_mean, diversity_mean, (0.4878048780487805, 0.1484821609214408)


In iteration 8

test_rating, baseline_rating, (49.31506849315068, 54.42942942942943)
rating_mean, diversity_mean, (0.5068493150684932, 0.07111797497884394)


In iteration 9

test_rating, baseline_rating, (46.666666666666664, 54.42942942942943)
rating_mean, diversity_mean, (0.5333333333333333, 0.10663262458044763)