import random
import numpy as np
import tensorflow as tf
import warnings
warnings.filterwarnings('ignore')

class ReplayMemory():

    '''
    Replay memory D in article
    '''

    def __init__(self, buffer_size):
        self.buffer_size = buffer_size
        self.buffer = []

    def add(self, state, action, reward, n_state):
        self.buffer.append([state, action, reward, n_state])
        if len(self.buffer) > self.buffer_size:
            self.buffer.pop(0)

    def size(self):
        return len(self.buffer)

    def sample_batch(self, batch_size):
        return random.sample(self.buffer, batch_size)


def experience_replay(replay_memory, batch_size, actor, critic, embeddings, ra_length, state_space_size,
                      action_space_size, discount_factor):
    '''
    Experience replay.
    Args:
          replay_memory: replay memory D in article.
          batch_size: sample size.
          actor: Actor network.
          critic: Critic network.
          embeddings: Embeddings object.
          state_space_size: dimension of states.
          action_space_size: dimensions of actions.
    Returns:
          Best Q-value, loss of Critic network for printing/recording purpose.
    '''
    print('replay start')
    # Sample minibatch of N transitions (s, a, r, s′)
    samples = replay_memory.sample_batch(batch_size)
    states = np.array([s[0] for s in samples])
    actions = np.array([s[1] for s in samples])
    rewards = np.array([s[2] for s in samples])
    n_states = np.array([s[3] for s in samples]).reshape(-1, state_space_size)

    # Generate a′ by target Actor network
    #     print(states,len(states))
    n_actions = actor.get_recommendation_list(ra_length, states, embeddings, target=True).reshape(-1, action_space_size)

    # Calculate predicted Q′(s′, a′|θ^µ′) value
    target_Q_value = critic.predict_target(n_states, n_actions, [ra_length] * batch_size)

    # Set y = r + γQ′(s′, a′|θ^µ′)'
    expected_rewards = rewards + discount_factor * target_Q_value

    # Update Critic by minimizing (y − Q(s, a|θ^µ))²'
    sequence_lengths_tensor = tf.constant([ra_length] * batch_size, dtype=tf.int32)
    critic_Q_value, critic_loss = critic.train(states, actions, sequence_lengths_tensor, expected_rewards)
    # Update the Actor using the sampled policy gradient'
    action_gradients = critic.get_action_gradients(states, n_actions, sequence_lengths_tensor)
    actor.train(states, sequence_lengths_tensor, action_gradients)

    # Update the Critic target networks
    critic.update_target_network()

    # Update the Actor target network'
    actor.update_target_network()

    print('replay end')

    return np.amax(critic_Q_value), critic_loss


class Noise():
    '''
    Noise for Actor predictions
    '''

    def __init__(self, action_space_size, mu=0, theta=0.5, sigma=0.2):
        self.action_space_size = action_space_size
        self.mu = mu
        self.theta = theta
        self.sigma = sigma
        self.state = np.ones(self.action_space_size) * self.mu

    def get(self):
        self.state += self.theta * (self.mu - self.state) + self.sigma * np.random.rand(self.action_space_size)
        return self.state
