import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Input, Dense, GRU, Reshape, Concatenate
from tensorflow.keras.losses import CategoricalCrossentropy, MeanSquaredError
import warnings
warnings.filterwarnings('ignore')

class Actor(Model):
    def __init__(self, state_space_size, action_space_size, batch_size, ra_length, history_length, embedding_size, tau, learning_rate, scope='actor'):
        super(Actor, self).__init__()
        self.state_space_size = state_space_size
        self.action_space_size = action_space_size
        self.batch_size = batch_size
        self.ra_length = ra_length
        self.history_length = history_length
        self.embedding_size = embedding_size
        self.tau = tau
        self.learning_rate = learning_rate
        self.scope = scope

        # Build Actor network
        self.model = self._build_net('estimator_actor')
        self.network_params = self._get_trainable_variables(scope)

        # Build target Actor network
        self.target_model = self._build_net('target_actor')
        self.target_network_params = self._get_trainable_variables(scope)

        # Initialize target network weights with network weights
        self.init_target_network_params = [self.target_network_params[i].assign(self.network_params[i])
                                           for i in range(len(self.target_network_params))]

        # Update target network weights self.update_target_network_params = [self.target_network_params[i].assign(
        # self.tau * self.network_params[i] + (1 - self.tau) * self.target_network_params[i]) for i in range(len(
        # self.target_network_params))]

        # Compute ∇_a.Q(s, a|θ^µ).∇_θ^π.f_θ^π(s)
        self.optimizer = tf.keras.optimizers.Adam(learning_rate=self.learning_rate)

    def _get_trainable_variables(self, scope):
        # print(f'In actor _get_trainable_variables')
        variables = [var for var in self.model.trainable_variables if scope in var.name]
        return variables

    def _build_net(self, scope):
        # print(f'In actor _build_net')
        state = tf.keras.Input(shape=(self.state_space_size,))
        state = tf.keras.Input(shape=(self.state_space_size,))
        state_ = tf.reshape(state, [-1, self.history_length, self.embedding_size])
        sequence_length = tf.keras.Input(shape=(), dtype=tf.int32, name='actor_sequence_length')
        cell = tf.keras.layers.GRU(self.embedding_size,
                                   activation=tf.nn.relu,
                                   kernel_initializer=tf.keras.initializers.RandomNormal(),
                                   bias_initializer=tf.keras.initializers.Zeros(),
                                   return_sequences=True)
        outputs = cell(state_, mask=tf.sequence_mask(sequence_length, maxlen=self.history_length))
        last_output = tf.gather(outputs, sequence_length - 1, axis=1)  # Gather the last output from each sequence
        x = tf.keras.layers.Dense(self.ra_length * self.embedding_size)(last_output)
        action_weights = tf.reshape(x, [-1, self.ra_length, self.embedding_size])
        model = tf.keras.Model(inputs=[state, sequence_length], outputs=action_weights)
        #         print(f'action_weights {action_weights} state {state} sequence_length {sequence_length} model {model}')
        return model

    def _compute_gradients(self, state, sequence_length, action_gradients):
        # print(f'In actor _compute_gradients')
        with tf.GradientTape() as tape:
            action_weights = self.model([state, sequence_length])
        grads = tape.gradient(action_weights, self.network_params, output_gradients=action_gradients)
        params_gradients = [grad / (self.batch_size * self.action_space_size) for grad in grads]
        #         print(f'params_gradients {params_gradients.shape}')
        return params_gradients

    def train(self, state, sequence_length, action_gradients):
        # print(f'In actor train')
        params_gradients = self._compute_gradients(state, sequence_length, action_gradients)
        # print(f'zip(params_gradients, self.network_params) {zip(params_gradients.shape, self.network_params.shape)}')
        self.optimizer.apply_gradients(zip(params_gradients, self.network_params))

    def predict(self, state, sequence_length):
        # print(f'In actor predict')
        sequence_length = np.array(sequence_length)
        # print(f'predicted val {self.model.predict([state, sequence_length]).shape}')
        return self.model.predict([state, sequence_length])

    def predict_target(self, state, sequence_length):
        # print(f'In actor predict target')
        sequence_length = np.array(sequence_length)
        # print(f'predicted val {self.target_model.predict([state, sequence_length]).shape}')
        return self.target_model.predict([state, sequence_length])

    def init_target_network(self):
        # print(f'In actor init_target_network')
        #         print(f'target_network params {self.target_network_params.shape}')
        for i in range(len(self.target_network_params)):
            self.target_network_params[i].assign(self.network_params[i])

    def update_target_network(self):
        # print(f'update_target_network')
        #         print(f'target_network params {self.target_network_params.shape}')
        for i in range(len(self.target_network_params)):
            self.target_network_params[i].assign \
                (self.tau * self.network_params[i] + (1 - self.tau) * self.target_network_params[i])

    def get_recommendation_list(self, ra_length, noisy_state, embeddings, target=False):
        '''
        Args:
          ra_length: length of the recommendation list.
          noisy_state: current/remembered environment state with noise.
          embeddings: Embeddings object.
          target: boolean to use Actor's network or target network.
        Returns:
          Recommendation List: list of embedded items as future actions.
        '''

        def get_score(weights, embedding, batch_size):
            '''
            Args:
            weights: w_t^k shape = (embedding_size,).
            embedding: e_i shape = (embedding_size,).
            Returns:
            score of the item i: score_i = w_t^k.e_i^T shape = (1,).
            '''
            ret = np.dot(weights, embedding.T)
            return ret

        batch_size = noisy_state.shape[0]
        #         batch_size = 64
        # Generate w_t = {w_t^1, ..., w_t^K}
        method = self.predict_target if target else self.predict
        weights = method(noisy_state, [ra_length] * batch_size)

        # Score items
        scores = np.array([[[get_score(weights[i][k], embedding, batch_size)
                             for embedding in embeddings.get_embedding_vector()]
                            for k in range(ra_length)] for i in range(batch_size)])

        # return a_t
        # print('In actor get_recommendation_list')
        #         print(f'[ra_length] * batch_size {([ra_length] * batch_size).shape}')
        #         print(f'scores {scores.shape}')
        x = np.array([[embeddings.get_embedding(np.argmax(scores[i][k]))
                       for k in range(ra_length)] for i in range(batch_size)])
        #         print(f'rec list {x}')
        return np.array([[embeddings.get_embedding(np.argmax(scores[i][k]))
                          for k in range(ra_length)] for i in range(batch_size)])


class Critic(Model):
    def __init__(self, state_space_size, action_space_size, history_length, embedding_size, tau, learning_rate,
                 scope='critic'):
        super(Critic, self).__init__()
        self.state_space_size = state_space_size
        self.action_space_size = action_space_size
        self.history_length = history_length
        self.embedding_size = embedding_size
        self.tau = tau
        self.learning_rate = learning_rate
        self.scope = scope

        # Model
        self.model = self._build_net()
        self.target_model = self._build_net()
        self.target_model.set_weights(self.model.get_weights())

        self.optimizer = Adam(learning_rate)
        self.mse = MeanSquaredError()

        self.network_params = self._get_trainable_variables(scope)

    def _get_trainable_variables(self, scope):
        # print('In Critic _get_trainable_variables')
        variables = [var for var in self.model.trainable_variables if scope in var.name]
        # print(f'variables {variables}')
        return variables

    def _build_net(self):
        '''
        Build the (target) Critic network
        '''
        # print('In critic _build_net')
        # Inputs: current state, current action
        state_input = Input(shape=(self.state_space_size,))
        state_input_reshaped = tf.reshape(state_input, [-1, self.history_length, self.embedding_size])
        action_input = Input(shape=(self.action_space_size,))
        sequence_length = tf.keras.Input(shape=(), dtype=tf.int32, name='critic_sequence_length')

        # Layers
        gru = GRU(self.history_length, activation=tf.nn.relu, kernel_initializer=tf.initializers.random_normal(),
                  recurrent_initializer=tf.zeros_initializer())(state_input_reshaped)
        concat = Concatenate()([gru, action_input])
        layer1 = Dense(32, activation=tf.nn.relu)(concat)
        layer2 = Dense(16, activation=tf.nn.relu)(layer1)
        critic_Q_value = Dense(1)(layer2)

        # Outputs: predicted Q-value
        model = Model(inputs=[state_input, action_input, sequence_length], outputs=critic_Q_value)

        return model

    #     def train(self, state, action, expected_reward):
    #         '''
    #         Minimize MSE between expected reward and target Critic's Q-value
    #         '''
    #         with tf.GradientTape() as tape:
    #             predicted_Q_value = self.model([state, action], training=True)
    #             loss = self.mse(expected_reward, predicted_Q_value)
    #         grads = tape.gradient(loss, self.model.trainable_variables)
    #         self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

    #         return predicted_Q_value, loss

    def train(self, state, action, sequence_length, expected_reward):
        '''
        Minimize MSE between expected reward and target Critic's Q-value
        '''
        # print('In critic train')
        with tf.GradientTape() as tape:
            critic_Q_value = self.model([state, action, sequence_length])
            #             critic_Q_value = self.model.predict([state, action, sequence_length])
            loss = tf.reduce_mean(tf.square(expected_reward - critic_Q_value))
        grads = tape.gradient(loss, self.network_params)
        self.optimizer.apply_gradients(zip(grads, self.network_params))
        # print(f'critic_Q_value {critic_Q_value.shape} loss {loss.shape}')
        return critic_Q_value, loss

    def predict(self, state, action, sequence_length):
        '''
        Returns Critic's predicted Q-value
        '''
        sequence_length = np.array(sequence_length)
        # print(f'In critic predict {self.model.predict([state, action, sequence_length]).shape}')
        return self.model.predict([state, action, sequence_length])

    def predict_target(self, state, action, sequence_length):
        '''
        Returns target Critic's predicted Q-value
        '''
        sequence_length = np.array(sequence_length)
        # print(f'In critic predict target {self.target_model.predict([state, action, sequence_length]).shape}')
        return self.target_model.predict([state, action, sequence_length])

    def get_action_gradients(self, state, action, sequence_length):
        '''
        Returns ∇_a.Q(s, a|θ^µ)
        '''
        # print(f'In critic get_action_gradients')
        # print(f'state {state.shape} action {action.shape}')
        action = tf.convert_to_tensor(action)
        # print(f' tesor action {action.shape}')
        with tf.GradientTape() as tape:
            #             tape.watch(action)
            predicted_Q_value = self.model([state, action, sequence_length], training=True)
        #         print(f'action gradients {tape.gradient(predicted_Q_value, action)}')
        return tape.gradient(predicted_Q_value, action)

    def init_target_network(self):
        # print(f'In critic init_target_network')
        #         print(f'target model weights {self.target_model.set_weights(self.model.get_weights())}')
        self.target_model.set_weights(self.model.get_weights())

    def update_target_network(self):
        weights = self.model.get_weights()
        target_weights = self.target_model.get_weights()
        # print(f'In critic update_target_network')
        #         print(f'weights {weights.shape}, target_weights {target_weights.shape}')
        for i in range(len(weights)):
            target_weights[i] = self.tau * weights[i] + (1 - self.tau) * target_weights[i]
        self.target_model.set_weights(target_weights)