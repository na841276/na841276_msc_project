import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import warnings

warnings.filterwarnings('ignore')
from config import *
from data_generation import DataGenerator
from embeddings import read_file, EmbeddingsGenerator, Embeddings, read_embeddings
from environment import Environment
from policy_networks import Actor, Critic
from agent import agent_train
import logging

logging.basicConfig(level=logging.DEBUG)


def main(alpha, ra_length):
    try:
        logging.debug('In main function, generating data')
        dg = DataGenerator(user_data_path, music_data_path)
        dg.gen_train_test(train_ratio=0.7, seed=42)
        dg.write_csv('train_set.csv', dg.train, nb_states=[history_length], nb_actions=[ra_length])
        dg.write_csv('test_set.csv', dg.test, nb_states=[history_length], nb_actions=[ra_length])
        data = read_file('train_set.csv')
        # Generate Embeddings and save
        logging.debug('Generating embeddings')
        eg = EmbeddingsGenerator(dg.user_train, dg.data)
        eg.train(nb_epochs=200)
        eg.save_embeddings('data/embeddings.csv')
        logging.debug('Saving embeddings')
        embeddings = Embeddings(read_embeddings('data/embeddings.csv'))
        state_space_size = embeddings.size() * history_length
        action_space_size = embeddings.size() * ra_length
        logging.debug('Initialising actor, critic and environment objects')
        actor = Actor(state_space_size, action_space_size, batch_size, ra_length, history_length, embeddings.size(),
                      tau,
                      actor_lr)
        critic = Critic(state_space_size, action_space_size, history_length, embeddings.size(), tau, critic_lr)
        environment = Environment(data, embeddings, alpha, gamma, fixed_length)
        logging.debug('Training the agent')
        agent_train(environment, actor, critic, embeddings, history_length, ra_length, buffer_size, batch_size,
                    discount_factor,
                    nb_episodes, filename_summary, nb_rounds)
        logging.debug('Convert state embeddings to items')
        dict_embeddings = {}
        for i, item in enumerate(embeddings.get_embedding_vector()):
            str_item = str(item)
            assert (str_item not in dict_embeddings)
            dict_embeddings[str_item] = i

        def state_to_items(state, actor, ra_length, embeddings, dict_embeddings, target=False):
            return [dict_embeddings[str(action)]
                    for action in
                    actor.get_recommendation_list(ra_length, np.array(state).reshape(1, -1), embeddings,
                                                  target).reshape(
                        ra_length, embeddings.size())]

        music_columns = ['acousticness', 'beat_strength', 'bounciness', 'danceability',
                         'energy', 'liveness', 'speechiness', 'valence']

        def test_actor(actor, test_df, all_df, embeddings, dict_embeddings, ra_length, history_length, target=False,
                       nb_rounds=2):
            ratings = []
            unknown = 0
            diversity = []

            for _ in range(nb_rounds):
                for i in range(len(test_df)):
                    history_sample = list(test_df[i].sample(history_length)['music_id'])
                    recommendation = state_to_items(embeddings.embed(history_sample), actor, ra_length, embeddings,
                                                    dict_embeddings, target)
                    features = []
                    for item in recommendation:
                        l = list(test_df[i].loc[test_df[i]['music_id'] == item]['rating'])
                        if len(l) == 0:
                            unknown += 1
                        else:
                            ratings.append(l[0])

                        features.append(all_df.loc[all_df['music_id'] == item][music_columns].iloc[0].tolist())

                    similarity = cosine_similarity(features)
                    upper_right = np.triu_indices(similarity.shape[0], k=1)
                    diversity.append(1 - np.mean(similarity[upper_right]))

            return ratings, diversity

        ratings, diversity = test_actor(actor, dg.test, dg.data, embeddings, dict_embeddings, ra_length, history_length,
                                        target=False, nb_rounds=2)
        rating_mean = np.mean(ratings)
        diversity_mean = np.mean(diversity)
        logging.debug(f'rating_mean {rating_mean}, diversity_mean = {diversity_mean}')
        test_rating, baseline_rating = (1 - rating_mean) * 100, (1 - dg.data.rating.mean()) * 100
        # test_rating = test_rating[0]
        # all_ratings = [test_rating, baseline_rating]
        logging.debug(f'test_rating {test_rating}, baseline_rating {baseline_rating}')
        return rating_mean, diversity_mean, test_rating, baseline_rating
    except Exception as e:
        logging.error(f'Exception occured with error {e}', exc_info=True)


if __name__ == '__main__':
    for x in range(10):
        with open('iteration_results.txt', 'a') as f:
            logging.debug(f'In iteration {x}')
            f.write(f'In iteration {x}\n\n')
            rating_mean, diversity_mean, test_rating, baseline_rating = main(alpha, ra_length)
            f.write(f'test_rating, baseline_rating, {test_rating, baseline_rating}\n')
            f.write(f'rating_mean, diversity_mean, {rating_mean, diversity_mean}\n')
            f.write("\n\n")

    for r in range(1, 7):
        ra_length = r
        with open('r_length_results.txt', 'a') as f:
            logging.debug(f'In ra_length {r}')
            f.write(f'In ra_length {r}\n\n')
            rating_mean, diversity_mean, test_rating, baseline_rating = main(alpha, ra_length)
            f.write(f'test_rating, baseline_rating, {test_rating, baseline_rating}\n')
            f.write(f'rating_mean, diversity_mean, {rating_mean, diversity_mean}\n')
            f.write("\n\n")

    alphas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]
    for a in alphas:
        alpha = a
        with open('alpha_results.txt', 'a') as f:
            logging.debug(f'In alpha {a}')
            f.write(f'In alpha {a}\n\n')
            rating_mean, diversity_mean, test_rating, baseline_rating = main(alpha, ra_length)
            f.write(f'test_rating, baseline_rating, {test_rating, baseline_rating}\n')
            f.write(f'rating_mean, diversity_mean, {rating_mean, diversity_mean}\n')
            f.write("\n\n")
