# Hyperparameters
history_length = 10 # N in session
ra_length = 3 # K in session
discount_factor = 0.99 # Gamma in Bellman equation
actor_lr = 0.00005
critic_lr = 0.001
tau = 0.001
batch_size = 64
nb_rounds = 50
nb_episodes = 5
alpha = 0.2
gamma = 0.9
buffer_size = 10000 # Size of replay memory D
filename_summary = 'summary.txt'
fixed_length = True # Fixed memory length
user_data_path = 'data/user_mini_data.tar.gz'
music_data_path = 'data/music_mini_data.tar.gz'