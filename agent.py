import logging
import time
import tensorflow as tf
import warnings

warnings.filterwarnings('ignore')
from experiance_replay import ReplayMemory, Noise, experience_replay

logging.basicConfig(level=logging.DEBUG)


def agent_train(environment, actor, critic, embeddings, history_length, ra_length, buffer_size, batch_size,
                discount_factor, nb_episodes, filename_summary, nb_rounds):
    # print('In agent train')
    logging.debug('In agent train')

    # Set up summary operators
    def build_summaries():
        episode_reward = tf.Variable(0.)
        episode_max_Q = tf.Variable(0.)
        critic_loss = tf.Variable(0.)

        summary_writer = tf.summary.create_file_writer(filename_summary)
        return summary_writer, episode_reward, episode_max_Q, critic_loss

    summary_writer, episode_reward, episode_max_Q, critic_loss = build_summaries()
    # print(f'episode_reward, episode_max_Q, critic_loss {episode_reward, episode_max_Q, critic_loss}')

    # Initialize target network
    # print('Initialize target network')
    actor.init_target_network()
    critic.init_target_network()

    # Initialize the capacity of replay memory
    # print('Initialize the capacity of replay memory')
    replay_memory = ReplayMemory(buffer_size)
    replay = False

    start_time = time.time()
    for i_session in range(nb_episodes):
        session_file = tf.summary.create_file_writer(f'session_{i_session}.txt')
        logging.debug(f'episode {i_session}')
        session_reward = 0
        session_Q_value = 0
        session_critic_loss = 0

        states = environment.reset()
        # print(f'states{states.shape}')

        if (i_session + 1) % 10 == 0:  # Update average parameters every 10 episodes
            environment.groups = environment.get_groups()
            # print(f'environment.groups {environment.groups.shape}')

        exploration_noise = Noise(history_length * embeddings.size())
        # print(f'exploration_noise {exploration_noise}')

        for t in range(nb_rounds):
            logging.debug(f'In episode {i_session}, round {t}')
            actions = actor.get_recommendation_list(
                ra_length,
                states.reshape(1, -1) + exploration_noise.get().reshape(1, -1),
                embeddings).reshape(ra_length, embeddings.size())
            #             actions = actor.get_recommendation_list(ra_length,states,embeddings).reshape(ra_length, embeddings.size())
            #             print(f'{t} actions {actions.shape}')
            rewards, next_states = environment.step(actions)
            # print(f'rewards, next_states {rewards.shape, next_states.shape}')
            replay_memory.add(states.reshape(history_length * embeddings.size()),
                              actions.reshape(ra_length * embeddings.size()),
                              [rewards],
                              next_states.reshape(history_length * embeddings.size()))

            states = next_states

            session_reward += rewards

            if replay_memory.size() >= batch_size:  # Experience replay
                replay = True
                replay_Q_value, critic_loss_value = experience_replay(replay_memory, batch_size,
                                                                      actor, critic, embeddings, ra_length,
                                                                      history_length * embeddings.size(),
                                                                      ra_length * embeddings.size(), discount_factor)
                session_Q_value += replay_Q_value
                session_critic_loss += critic_loss_value

            with summary_writer.as_default():
                tf.summary.scalar('reward', session_reward, step=i_session)
                tf.summary.scalar('max_Q_value', session_Q_value, step=i_session)
                tf.summary.scalar('critic_loss', session_critic_loss, step=i_session)

            with session_file.as_default():
                tf.summary.scalar('reward', session_reward, step=t)
                tf.summary.scalar('max_Q_value', session_Q_value, step=t)
                tf.summary.scalar('critic_loss', session_critic_loss, step=t)

            if (i_session == 1) and (t == 1):
                x = 'w'
            else:
                x = 'a'
            with open('results', x) as f:
                f.write(f'In episode {i_session}, round {t}\n\n')
                f.write(f'reward, {session_reward}\n')
                f.write(f'max_Q_value, {session_Q_value}\n')
                f.write(f'critic_loss, {session_critic_loss}\n')
                f.write("\n\n")
        # str_loss = 'Loss = %0.4f' % session_critic_loss
        str_loss = f'Loss = {session_critic_loss:.4f}'
        # print(('Episode %d/%d Time = %ds ' + (str_loss if replay else 'No replay')) % (
        # i_session + 1, nb_episodes, time.time() - start_time))
        logging.debug(('Episode %d/%d Time = %ds ' + (str_loss if replay else 'No replay')) % (
            i_session + 1, nb_episodes, time.time() - start_time))
        start_time = time.time()

    summary_writer.close()
